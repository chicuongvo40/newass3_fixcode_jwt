﻿using BusinessObjects.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class UserDAO
    {
        public static List<AspNetUser> GetUsers()
        {
            var listUser = new List<AspNetUser>();
            try
            {
                using (var context = new MyStoreDBContext())
                {
                    listUser = context.AspNetUsers.ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return listUser;
        }

        public static List<AspNetUser> Search(string keyword)
        {
            var listUser = new List<AspNetUser>();
            try
            {
                using (var context = new MyStoreDBContext())
                {
                    listUser = context.AspNetUsers
                        .Where(c => c.UserName.Contains(keyword))
                        .ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return listUser;
        }

        public static void SaveUser(AspNetUser user)
        {
            try
            {
                using (var context = new MyStoreDBContext())
                {
                    context.AspNetUsers.Add(user);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static AspNetUser FindUserById(int userId)
        {
            var _user = new AspNetUser();
            try
            {
                using (var context = new MyStoreDBContext())
                {
                    _user = context.AspNetUsers.SingleOrDefault(c => c.Id == userId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return _user;
        }

        public static AspNetUser FindUserByEmail(string email)
        {
            var customer = new AspNetUser();
            try
            {
                using (var context = new MyStoreDBContext())
                {
                    customer = context.AspNetUsers.FirstOrDefault(c => c.Email == email);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return customer;
        }

        public static void UpdateUser(AspNetUser User)
        {
            try
            {
                using (var context = new MyStoreDBContext())
                {
                    context.Entry(User).State =
                        Microsoft.EntityFrameworkCore.EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void DeleteUser(AspNetUser customer)
        {
            try
            {
                using (var context = new MyStoreDBContext())
                {
                    var customerToDelete = context
                        .AspNetUsers
                        .SingleOrDefault(c => c.Id == customer.Id);
                    context.AspNetUsers.Remove(customerToDelete);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

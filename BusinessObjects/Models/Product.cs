﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{

    public partial class Product
    {
        public Product(int productId, int? categoryId, string productName, string weight, decimal unitPrice, string unitslnStock)
        {
            ProductId = productId;
            CategoryId = categoryId;
            ProductName = productName;
            Weight = weight;
            UnitPrice = unitPrice;
            UnitslnStock = unitslnStock;
        }

        public Product()
        {
        }

        public int ProductId { get; set; }
        public int? CategoryId { get; set; }
        public string ProductName { get; set; } = null!;
        public string Weight { get; set; } = null!;
        public decimal UnitPrice { get; set; }
        public string UnitslnStock { get; set; } = null!;

        public virtual Category? Category { get; set; }
    }
}

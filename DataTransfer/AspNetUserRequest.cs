﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransfer
{
    public class AspNetUserRequest
    {
        public int Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string NormalizedUserName { get; set; }
       
        [Required]
        public string ConcurrencyStamp { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string NormalizedEmail { get; set; }
        [Required]
        public string EmailConfirmed { get; set; }
        [Required]
        public string SecurityStamp { get; set; }
        [Required]
        public string LockoutEnd { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string PhoneNumberConfirmed { get; set; }
        [Required]
        public string TwoFactorEnabled { get; set; }
        [Required]
        public string LockoutEnabled { get; set; }
        [Required]
        public string AccessFailedCount { get; set; }
        [MinLength(5)]
        [MaxLength(255)]
        public string? PasswordHash { get; set; }
    }
}

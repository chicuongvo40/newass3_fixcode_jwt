﻿using BusinessObjects.Models;
using Microsoft.AspNetCore.Mvc;
using Repositories.Interface;
using Repositories;
using Microsoft.AspNetCore.Authorization;
using DataTransfer;

namespace IdentityAJaxClient.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private ICategoryRepository repository = new CategoryRepository();

        [HttpGet]
        public ActionResult<IEnumerable<Category>> GetCategories() => repository.GetCategories();

        [HttpPost]
        public IActionResult PostCategory(CategoryRequest category)
        {
            var p = new Category
            {
                CategoryId = category.CategoryId,
                CategoryName = category.CategoryName,
            };
            repository.SaveCategory(p);
            return NoContent();
        }
    }
}